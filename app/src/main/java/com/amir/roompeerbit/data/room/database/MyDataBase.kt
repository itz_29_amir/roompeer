package com.amir.roompeerbit.data.room.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.amir.roompeerbit.data.model.Person
import com.amir.roompeerbit.data.room.dao.PersonDao

@Database(entities = [Person::class], version = 1, exportSchema = false)
abstract class MyDataBase : RoomDatabase() {

    abstract fun personDao() : PersonDao

    companion object {

        private var INSTANCE :MyDataBase?=null
        fun getDatabase(context: Context): MyDataBase{
            val instance = Room.databaseBuilder(
                context.applicationContext,
                MyDataBase::class.java,
                "category_database"
            ).build()
            return instance
        }

    }
}