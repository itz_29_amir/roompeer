package com.amir.roompeerbit.data.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.amir.roompeerbit.data.model.Person
import com.amir.roompeerbit.data.repository.PersonRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class PersonViewModel @Inject constructor(var personRepository: PersonRepository) : ViewModel() {
    val readAllData: LiveData<List<Person>> = personRepository.readAllData


    fun insertPerson(person: Person){
        viewModelScope.launch(Dispatchers.IO) {
            personRepository.insertPerson(person)
        }
    }


    fun updatePerson(person: Person){
        viewModelScope.launch(Dispatchers.IO) {
            personRepository.updatePerson(person)
        }
    }


    fun deletePerson(person: Person){
        viewModelScope.launch(Dispatchers.IO) {
            personRepository.deletePerson(person)
        }
    }


    fun deleteAllPerson(){
        viewModelScope.launch(Dispatchers.IO) {
            personRepository.deleteAllPerson()
        }
    }
}