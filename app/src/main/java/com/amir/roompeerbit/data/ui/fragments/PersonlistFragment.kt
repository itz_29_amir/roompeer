package com.amir.roompeerbit.data.ui.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.NavDirections
import androidx.navigation.Navigation
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.amir.roompeerbit.MainActivity
import com.amir.roompeerbit.data.ui.adapter.PersonListAdapter
import com.amir.roompeerbit.data.viewmodel.PersonViewModel
import com.amir.roompeerbit.databinding.FragmentPersonlistBinding
import com.amir.roompeerbit.utils.Type
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class PersonlistFragment : Fragment() {

    private val personViewModel : PersonViewModel by viewModels()
    private val args by navArgs<PersonlistFragmentArgs>()

    private lateinit var binding: FragmentPersonlistBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding= FragmentPersonlistBinding.inflate(layoutInflater,container,false)

        init();
        return binding.root
    }

    private fun init() {
        binding.llAddPersons.setOnClickListener(
            View.OnClickListener {
                val directions: NavDirections =
                    PersonlistFragmentDirections.actionPersonlistFragmentToAddPersonFragment(Type.ADD,null)
                Navigation.findNavController(binding.rvPersonList).navigate(directions)

            }
        )

        val adapter = PersonListAdapter({delete->
            personViewModel.deletePerson(delete)
        },{update->

            val directions: NavDirections =
                PersonlistFragmentDirections.actionPersonlistFragmentToAddPersonFragment(Type.UPDATE,update)
            Navigation.findNavController(binding.rvPersonList).navigate(directions)

        })

        binding.rvPersonList.layoutManager = LinearLayoutManager(requireContext())
        binding.rvPersonList.adapter = adapter

        personViewModel.readAllData.observe(viewLifecycleOwner,{
            adapter.setData(it)
        })

    }


}