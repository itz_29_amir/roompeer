package com.amir.roompeerbit.data.ui.fragments

import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavDirections
import androidx.navigation.Navigation
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.navArgs
import com.amir.roompeerbit.MainActivity
import com.amir.roompeerbit.R
import com.amir.roompeerbit.data.model.Person
import com.amir.roompeerbit.data.viewmodel.PersonViewModel
import com.amir.roompeerbit.databinding.FragmentAddPersonBinding
import com.amir.roompeerbit.utils.Type
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

@AndroidEntryPoint
class AddPersonFragment : Fragment() {
    private val personViewModel : PersonViewModel by viewModels()
    private val args by navArgs<AddPersonFragmentArgs>()

    private lateinit var binding: FragmentAddPersonBinding

    private var type = Type.ADD
    private var person : Person?= null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding= FragmentAddPersonBinding.inflate(layoutInflater,container,false)


        init()

        return binding.root
    }

    private fun init() {

        type=args.type


        binding.back.setOnClickListener(View.OnClickListener {
//                Navigation.findNavController(requireView()).popBackStack()
            Log.i("vxvx", "init: "+13)
            NavHostFragment.findNavController(this).navigateUp()
        })


        if (type==Type.UPDATE){
            person=args.person
            binding.etName.setText(person?.name)
            binding.etAge.setText(person?.age)
            binding.etDesignation.setText(person?.designation)
            binding.btnDone?.setText("Update")

            Log.i("vxvx", "init: "+2425)

        }
        binding.btnDone.setOnClickListener(View.OnClickListener {view->

            if (validate()){
                insertAndUpdateData()

                val directions: NavDirections = AddPersonFragmentDirections.actionAddPersonFragmentToPersonlistFragment(binding.etName.text.toString(),
                    binding.etAge.text.toString(),binding.etDesignation.text.toString())
                Navigation.findNavController(view).navigate(directions)

            }

        })
    }


    private fun validate(): Boolean {
         return if (TextUtils.isEmpty(binding.etName.text.toString().replace(" ", ""))) {
             Snackbar.make(
                 binding.root,
                 "enter name",
                 Snackbar.LENGTH_SHORT
             ).show()
             false
         } else if (TextUtils.isEmpty(binding.etAge.getText().toString().replace(" ", ""))) {
             Snackbar.make(
                 binding.root,
                 "enter age",
                 Snackbar.LENGTH_SHORT
             ).show()
             false
         }  else if (TextUtils.isEmpty(binding.etDesignation.getText().toString().replace(" ", ""))) {
             Snackbar.make(
                 binding.root,
                 "enter designation",
                 Snackbar.LENGTH_SHORT
             ).show()
             false
         } else {
             true
         }
     }

    private fun insertAndUpdateData() {


        lifecycleScope.launch {

            val name = binding.etName.text.toString()
            val age = binding.etAge.text.toString()
            val designation = binding.etDesignation.text.toString()


                binding.progressBar.visibility=View.VISIBLE
                if(type == Type.ADD){
                    val person = Person(0,name,age,designation)
                    personViewModel.insertPerson(person)
                    Toast.makeText(requireContext(), "Successfully Added", Toast.LENGTH_LONG).show()
                }
                else{
                    val person = Person(person?.Pid!!,name,age,designation)
                    personViewModel.updatePerson(person)
                    Toast.makeText(requireContext(), "Successfully Updated", Toast.LENGTH_LONG)
                        .show()
                }


        }
    }

//    private fun inputCheck(name: String, age: String,designation : String): Boolean {
//        return (name.isNotEmpty() && description.isNotEmpty())
//    }

}