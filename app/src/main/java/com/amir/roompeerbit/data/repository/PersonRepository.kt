package com.amir.roompeerbit.data.repository

import androidx.lifecycle.LiveData
import com.amir.roompeerbit.data.model.Person
import com.amir.roompeerbit.data.room.dao.PersonDao
import javax.inject.Inject

class PersonRepository @Inject constructor(private val personDao: PersonDao) {

    val readAllData : LiveData<List<Person>> =personDao.readAllData()

    suspend fun insertPerson(person: Person){
        personDao.insertPerson(person)
    }

    suspend fun updatePerson(person: Person){
        personDao.updatePerson(person)
    }

    suspend fun deletePerson(person: Person){
        personDao.deletePerson(person)
    }

    suspend fun deleteAllPerson(){
        personDao.deleteAllPersons()
    }
}