package com.amir.roompeerbit.data.model

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity
data class Person(
    @PrimaryKey(autoGenerate = true)
    var Pid : Int,
    var name : String,
    var age : String,
    var designation : String
): Parcelable
