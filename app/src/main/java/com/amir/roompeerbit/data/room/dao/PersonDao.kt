package com.amir.roompeerbit.data.room.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.amir.roompeerbit.data.model.Person

@Dao
interface PersonDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertPerson(person: Person)

    @Update
    suspend fun updatePerson(person: Person)

    @Delete
    suspend fun deletePerson(person: Person)


    @Query("DELETE FROM Person")
    suspend fun deleteAllPersons(): Int


    @Query("SELECT * FROM Person ORDER BY Pid ASC")
    fun readAllData(): LiveData<List<Person>>
}