package com.amir.roompeerbit.data.ui.adapter

import android.app.AlertDialog
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.PopupMenu
 import androidx.recyclerview.widget.RecyclerView
import com.amir.roompeerbit.R
import com.amir.roompeerbit.data.model.Person
import com.amir.roompeerbit.databinding.PersonlistBinding

class PersonListAdapter(val delete: (Person)->Unit,
                        val update: (Person)->Unit)
    : RecyclerView.Adapter<PersonListAdapter.PersonListAdapter_View>() {

    private var personlist = emptyList<Person>()

    class PersonListAdapter_View(val binding: PersonlistBinding) :
        RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PersonListAdapter_View {
        val binding =
            PersonlistBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return PersonListAdapter_View(binding)
    }

    override fun onBindViewHolder(holder: PersonListAdapter_View, position: Int) {
        val currentItem = personlist[position]
        holder.binding.name.text = currentItem.name
        holder.binding.age.text = currentItem.age
        holder.binding.designation.text = currentItem.designation

        holder.binding.options.setOnClickListener {
            showPopUpMenu(it, position)
        }
    }


    fun showPopUpMenu(view: View?, position: Int) {
        val currentitem: Person = personlist.get(position)
        val popupMenu = PopupMenu(view?.context, view)
        popupMenu.menuInflater.inflate(R.menu.menu, popupMenu.menu)
        popupMenu.setOnMenuItemClickListener { item: MenuItem ->
            when (item.itemId) {
                R.id.menuDelete -> {
                    val alertDialogBuilder =
                        AlertDialog.Builder(view?.context, R.style.AppTheme_Dialog)
                    alertDialogBuilder.setTitle("Confirmation")
                        .setMessage("Are you sure you want to delete this category")
                        .setPositiveButton("Yes") { dialog, which ->
                            delete(currentitem)
                        }
                        .setNegativeButton("No") { dialog, which -> dialog.cancel() }.show()
                }
                R.id.menuUpdate -> {
                    update(currentitem)
                }
            }
            false
        }
        popupMenu.show()
    }


    override fun getItemCount(): Int {
        return personlist.size
    }

    fun setData(personlist: List<Person>) {
        this.personlist = personlist
        notifyDataSetChanged()

    }
}