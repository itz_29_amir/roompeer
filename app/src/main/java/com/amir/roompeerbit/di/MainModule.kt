package com.amir.roompeerbit.di

import android.content.Context
import com.amir.roompeerbit.data.room.dao.PersonDao
import com.amir.roompeerbit.data.room.database.MyDataBase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class MainModule {

    @Provides
    @Singleton
    fun getPersonDao(myDataBase: MyDataBase): PersonDao = myDataBase.personDao()

    @Provides
    @Singleton
    fun getMyRoomDataBase(@ApplicationContext appContext: Context) : MyDataBase = MyDataBase.getDatabase(appContext)

}